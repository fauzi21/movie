import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'core/app_router.gr.dart';
import 'core/theme/theme_data.dart';
import 'features/home/screen/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: AutoRouterDelegate(_appRouter),
      routeInformationParser: _appRouter.defaultRouteParser(),
      theme: mainTheme,
    );
  }
}
