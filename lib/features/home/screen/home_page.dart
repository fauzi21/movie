import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:movies/core/app_router.gr.dart';
import 'package:movies/core/widgets/my_scaffold.dart';
import 'package:movies/features/form/model/movie_model.dart';
import 'package:movies/features/home/model/movies.dart';
import 'package:movies/features/home/screen/home_view.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final Movies movies = Movies();
  late StackRouter router;

  void _goToFormMovie(MovieModel? data) {
    router.push(FormMoviePageRoute(data: data)).then((value) {
      if (value != null) {
        setState(() {
          final values = value as List;
          // true is delete
          if (values.first) {
            movies.deleteMovie(values.last);
          } else {
            movies.addMovie(value.last as MovieModel);
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    router = AutoRouter.of(context);

    return MyScaffold(
      title: 'Movies',
      menus: [
        MaterialButton(
          child: const Text('New',
            style: TextStyle(
              color: Colors.white
            ),
          ),
          onPressed: () => _goToFormMovie(null),
        )
      ],
      body: HomeView(
        onSelect: (item) => _goToFormMovie(item),
        movies: movies,
      ),
    );
  }
}
