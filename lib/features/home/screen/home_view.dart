import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movies/core/theme/theme_value.dart';
import 'package:movies/core/widgets/separator_widget.dart';
import 'package:movies/features/form/model/movie_model.dart';
import 'package:movies/features/home/model/movies.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key, required this.onSelect, required this.movies}) : super(key: key);

  final Function(MovieModel) onSelect;
  final Movies movies;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (ctx) {
        if (movies.movies.isNotEmpty) {
          movies.movies.sort((a, b) {
            return DateTime.parse(b.id!).compareTo(DateTime.parse(a.id!));
          });
        }
        return ListView.separated(
          itemCount: movies.movies.length,
          separatorBuilder: (ctx, idx) => const SeparatorWidget(),
          itemBuilder: (context, index) {
            final movieItem = movies.movies[index];

            return MaterialButton(
              onPressed: () => onSelect(movieItem),
              padding: const EdgeInsets.all(paddingNormal),
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(movieItem.title ?? '',
                      style: const TextStyle(
                          fontSize: fontLarge,
                          fontWeight: FontWeight.w600
                      ),
                    ),

                    Text(movieItem.director ?? '',
                      style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
