class MovieItem {

  final String title;
  final String director;

  MovieItem(this.title, this.director);
}