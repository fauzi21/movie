import 'package:mobx/mobx.dart';
import 'package:movies/features/form/model/movie_model.dart';

part 'movies.g.dart';

class Movies = MoviesBase with _$Movies;

abstract class MoviesBase with Store {

  @observable
  ObservableList<MovieModel> movies = ObservableList.of([]);

  @action
  void addMovie(MovieModel movie) {
    // update list movies
    if (movie.id != null) {
      movies.removeWhere((element) => element.id == movie.id);
      movies.add(movie);
    } else {
      movies.add(movie.copyWith(id: DateTime.now().toIso8601String()));
    }
  }

  void deleteMovie(MovieModel movie) {
    // delete movie
    movies.remove(movie);
  }
}