enum TagsEnum {
  action,
  comedy,
  fantasy,
  horror,
  sci_fi
}

extension TagsValues on TagsEnum {
  String get value {
    switch (this) {
      case TagsEnum.action:
        return 'Action';
      case TagsEnum.comedy:
        return 'Comedy';
      case TagsEnum.fantasy:
        return 'Fantasy';
      case TagsEnum.horror:
        return 'Horror';
      case TagsEnum.sci_fi:
        return 'Sci-fi';
    }
  }
}