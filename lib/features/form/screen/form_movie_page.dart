import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:movies/core/widgets/dropdown_field.dart';
import 'package:movies/core/widgets/menu_button.dart';
import 'package:movies/core/widgets/my_scaffold.dart';
import 'package:movies/core/widgets/string_field.dart';
import 'package:movies/features/form/enum/tags_enum.dart';
import 'package:movies/features/form/model/movie_model.dart';

class FormMoviePage extends StatefulWidget {
  const FormMoviePage({Key? key, this.data}) : super(key: key);

  final MovieModel? data;

  @override
  State<FormMoviePage> createState() => _FormMoviePageState();
}

class _FormMoviePageState extends State<FormMoviePage> {

  late StackRouter router;
  MovieModel movie = MovieModel();

  void _onSave() {
    router.pop([false, movie]);
  }

  void _onDelete() {
    router.pop([true, movie]);
  }

  @override
  void initState() {
    if (widget.data != null) {
      movie = widget.data!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    router = AutoRouter.of(context);

    return MyScaffold(
      title: '${widget.data == null ? 'New' : 'Update'} Movie',
      isUseLeadingButton: false,
      menus: menus(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            StringField(
              title: 'Title',
              initialValue: movie.title,
              onChanged: (value) {
                movie = movie.copyWith(title: value);
              },
            ),

            StringField(
              title: 'Director',
              initialValue: movie.director,
              onChanged: (value) {
                movie = movie.copyWith(director: value);
              },
            ),

            DropdownField(
              items: TagsEnum.values.map((e) => e.value).toList(),
              lable: 'Tags',
              initialValue: movie.tags,
              onChanged: (value) {
                movie = movie.copyWith(tags: value);
              },
            ),

            StringField(
              title: 'Summary',
              initialValue: movie.summary,
              onChanged: (value) {
                movie = movie.copyWith(summary: value);
              },
            )
          ],
        ),
      ),
    );
  }

  List<Widget> menus() {
    if (widget.data == null) {
      return [
        MenuButton(
          onPressed: () => _onSave(),
          title: 'Save',
        )
      ];
    }

    return List.generate(2, (index) {
      return MenuButton(
        onPressed: () => index == 0 ? _onDelete() : _onSave(),
        title: index == 0 ? 'Delete' : 'Update',
      );
    }).toList();
  }
}

