import 'package:mobx/mobx.dart';

class MovieModel {

  final String? id;
  final String? title;
  final String? director;
  final String? summary;
  final String? tags;

  MovieModel({this.id, this.title, this.director, this.summary, this.tags});

  MovieModel copyWith({
    String? id,
    String? title,
    String? director,
    String? summary,
    String? tags
  }) => MovieModel(
    id: id ?? this.id,
    title: title ?? this.title,
    director: director ?? this.director,
    summary: summary ?? this.summary,
    tags: tags ?? this.tags
  );

  @override
  String toString() {
    return 'MovieModel{id: $id, title: $title, director: $director, summary: $summary, tags: $tags}';
  }
}