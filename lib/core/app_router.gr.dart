// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;
import 'package:movies/features/form/model/movie_model.dart' as _i5;
import 'package:movies/features/form/screen/form_movie_page.dart' as _i4;
import 'package:movies/features/home/screen/home_page.dart' as _i3;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    HomePageRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i3.HomePage());
    },
    FormMoviePageRoute.name: (routeData) {
      final args = routeData.argsAs<FormMoviePageRouteArgs>(
          orElse: () => const FormMoviePageRouteArgs());
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData,
          child: _i4.FormMoviePage(key: args.key, data: args.data));
    }
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(HomePageRoute.name, path: '/'),
        _i1.RouteConfig(FormMoviePageRoute.name, path: '/form-movie-page')
      ];
}

class HomePageRoute extends _i1.PageRouteInfo<void> {
  const HomePageRoute() : super(name, path: '/');

  static const String name = 'HomePageRoute';
}

class FormMoviePageRoute extends _i1.PageRouteInfo<FormMoviePageRouteArgs> {
  FormMoviePageRoute({_i2.Key? key, _i5.MovieModel? data})
      : super(name,
            path: '/form-movie-page',
            args: FormMoviePageRouteArgs(key: key, data: data));

  static const String name = 'FormMoviePageRoute';
}

class FormMoviePageRouteArgs {
  const FormMoviePageRouteArgs({this.key, this.data});

  final _i2.Key? key;

  final _i5.MovieModel? data;
}
