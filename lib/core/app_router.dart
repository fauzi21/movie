import 'package:auto_route/annotations.dart';
import 'package:movies/features/form/screen/form_movie_page.dart';
import 'package:movies/features/home/screen/home_page.dart';

@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: FormMoviePage),
  ],
)

class $AppRouter {}