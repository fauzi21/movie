import 'package:flutter/material.dart';
import 'package:movies/core/theme/theme_value.dart';
import 'package:movies/features/form/enum/tags_enum.dart';

class DropdownField extends StatelessWidget {
  const DropdownField({Key? key, this.lable, required this.items, this.onChanged, this.initialValue}) : super(key: key);

  final String? lable;
  final String? initialValue;
  final List<String> items;
  final Function(dynamic)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(paddingNormal),
      child: DropdownButtonFormField(
        items: items.map((e) => DropdownMenuItem(child: Text(e), value: e,)).toList(),
        value: initialValue,
        decoration: InputDecoration(
            label: Text(lable ?? ''),
            contentPadding: const EdgeInsets.symmetric(
                horizontal: paddingMedium
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
                borderSide: BorderSide(
                    color: disableColors,
                    width: 1
                )
            )
        ),
        onChanged: onChanged,
      ),
    );
  }
}
