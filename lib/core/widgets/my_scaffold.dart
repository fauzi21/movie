import 'package:flutter/material.dart';

class MyScaffold extends StatelessWidget {
  const MyScaffold({
    Key? key,
    required this.body,
    required this.title,
    this.menus,
    this.isUseLeadingButton
  }) : super(key: key);

  /// [AppBar] title
  final String title;

  /// Body of [Scaffold]
  final Widget body;

  /// Create menu on [AppBar]
  final List<Widget>? menus;

  /// Activate leading back button
  final bool? isUseLeadingButton;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: isUseLeadingButton ?? true,
        actions: menus,
        centerTitle: false,
        title: Text(title),
      ),
      body: body,
    );
  }
}
