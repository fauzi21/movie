import 'package:flutter/material.dart';
import 'package:movies/core/theme/theme_value.dart';

class StringField extends StatelessWidget {
  const StringField({Key? key, this.title, this.onChanged, this.initialValue}) : super(key: key);

  final String? title;
  final String? initialValue;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(paddingNormal),
      child: TextFormField(
        initialValue: initialValue,
        decoration: InputDecoration(
            label: Text(title ?? ''),
            contentPadding: const EdgeInsets.symmetric(
                horizontal: paddingMedium
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
                borderSide: BorderSide(
                    color: disableColors,
                    width: 1
                )
            )
        ),
        onChanged: onChanged,
      ),
    );
  }
}
