import 'package:flutter/material.dart';
import 'package:movies/core/theme/theme_value.dart';

ThemeData get mainTheme {
  return ThemeData(
    primaryColor: primaryColors,
    disabledColor: disableColors
  );
}