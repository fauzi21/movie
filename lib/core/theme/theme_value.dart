import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const double fontLarge = 22;
const double fontMedium = 16;
const double fontNormal = 14;
const double fontSmall = 10;

const double paddingNormal = 12;
const double paddingMedium = 16;

/// colors
Color disableColors = Colors.grey.shade600;
Color primaryColors = Colors.blue;